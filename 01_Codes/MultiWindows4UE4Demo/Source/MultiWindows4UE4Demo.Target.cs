// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class MultiWindows4UE4DemoTarget : TargetRules
{
	public MultiWindows4UE4DemoTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "MultiWindows4UE4Demo" } );
	}
}
